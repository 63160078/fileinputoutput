/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.fileinputoutput;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Ow
 */
public class Computer {
    private int comHand; //computerHand rock=0 ;; scissors=1 ;; paper=2
    private int playerHand; //playerHand rock=0 ;; scissors=1 ;; paper=2
    private int win, lose, draw;
    private int status;
    
    public Computer() {
        
    }
    
    private int choob() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int paoYingChoob(int playerHand) { //win: 1, draw:0, lose:-1
        this.playerHand = playerHand;
        this.comHand = choob();
        if(this.playerHand == this.comHand) {
            draw++;
            status=0;
            return 0;
        }
        if(this.playerHand == 0 && this.comHand == 1){
            win++;
            status=1;
            return 1;
        }
        if(this.playerHand == 1 && this.comHand == 2){
            win++;
            status=1;
            return 1;
        }
        if(this.playerHand == 2 && this.comHand == 0){
            win++;
            status=1;
            return 1;
        }
        lose++;
        status=-1;
        return -1;
    }


    public int getStatus() {
        return status;
    }

    public int getcomHand() {
        return comHand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }
    
    
}
